<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/buapmx/templates/page--front.html.twig */
class __TwigTemplate_d3d1462e6d425b5148a093c8c0c0bc86 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@bootstrap_barrio/layout/page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@bootstrap_barrio/layout/page.html.twig", "themes/custom/buapmx/templates/page--front.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 71
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 72
        echo "
<header class=\"l-menu-top ";
        // line 73
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null), 73, $this->source), "html", null, true);
        echo "\">
\t<div class=\"row\">
\t\t<div class=\"col-12\">
\t\t\t<img src=\"";
        // line 76
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null), 76, $this->source) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null), 76, $this->source)), "html", null, true);
        echo "/img/escudo_blanco.png\">
\t\t\t";
        // line 77
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "menutop", [], "any", false, false, true, 77), 77, $this->source), "html", null, true);
        echo "
\t\t</div>
\t</div>
</header>
";
    }

    // line 83
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 84
        echo "<div id=\"main\" class=\"";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null), 84, $this->source), "html", null, true);
        echo "\">
\t<div class=\"l-slider-principal\">
\t\t";
        // line 86
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sliderprincipal", [], "any", false, false, true, 86), 86, $this->source), "html", null, true);
        echo "
\t</div>         
\t<div class=\"l-busqueda\">
\t\t";
        // line 89
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "busqueda", [], "any", false, false, true, 89), 89, $this->source), "html", null, true);
        echo "
\t</div>
\t<div class=\"l-slider-intermedio\">
\t\t";
        // line 92
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sliderintermedio", [], "any", false, false, true, 92), 92, $this->source), "html", null, true);
        echo "
\t</div>
\t<div class=\"l-historias\">
\t\t";
        // line 95
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "historias", [], "any", false, false, true, 95), 95, $this->source), "html", null, true);
        echo "
\t</div>
\t<div class=\"l-noticias\">
\t\t";
        // line 98
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "noticias", [], "any", false, false, true, 98), 98, $this->source), "html", null, true);
        echo "
\t</div>
\t<div class=\"l-video\">
\t\t";
        // line 101
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "video", [], "any", false, false, true, 101), 101, $this->source), "html", null, true);
        echo "
\t</div>
\t<div class=\"l-eventos\">
\t\t";
        // line 104
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "eventos", [], "any", false, false, true, 104), 104, $this->source), "html", null, true);
        echo "
\t</div>
\t<div class=\"l-convocatorias\">
\t\t";
        // line 107
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "convocatorias", [], "any", false, false, true, 107), 107, $this->source), "html", null, true);
        echo "
\t</div>
</div>
";
    }

    // line 112
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 113
        echo "<footer class=\"footer\">\t
 \t";
        // line 114
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 114), 114, $this->source), "html", null, true);
        echo "
 \t
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/buapmx/templates/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 114,  140 => 113,  136 => 112,  128 => 107,  122 => 104,  116 => 101,  110 => 98,  104 => 95,  98 => 92,  92 => 89,  86 => 86,  80 => 84,  76 => 83,  67 => 77,  63 => 76,  57 => 73,  54 => 72,  50 => 71,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/buapmx/templates/page--front.html.twig", "/var/www/html/buap/buap-2022/web/themes/custom/buapmx/templates/page--front.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array();
        static $filters = array("escape" => 73);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
