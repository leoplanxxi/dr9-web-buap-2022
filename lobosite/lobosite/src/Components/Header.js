import React,{Component} from 'react';
import escudo from '../Assets/img/escudo_blanco.png';

class Header extends Component {
	render() {
		return (
			<div class="container-fluid container-header">
				<div class="row">
					<div class="col-12">
						<img src={escudo} />
					</div>
				</div>
			</div>	
		);
	}
}

export default Header;
