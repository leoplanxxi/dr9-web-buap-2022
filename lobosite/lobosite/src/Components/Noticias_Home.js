import React, {Component} from 'react';

/*json.forEach(function(e){
				url_img = e.field_imagen_slider_principal[0].url;	
			});*/
class Noticias_Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			items: []
		};
	}
	
	componentDidMount() {
		fetch(global.config.variables.boletines_url)
		.then(function(response){
			return response.json();
		})
		.then(
			(json) => {
				this.setState({
					isLoaded: true,
					items: json,
				});
			}
		);

	}

	render() {
		const { error, isLoaded, items} = this.state;
		if (error) {
			return <div>Error: {error.message}</div>;
		}
		else if (!isLoaded) {
			return <div>Cargando...</div>;
		}
		else {
			return(
				<div class="row row-noticias">
					<h1 class="titulo-noticias">Noticias</h1>
					<div class="col-lg-12 col-noticias">
						<div class="row">

						{items.nodes.map(item=>(
							<div class="col-lg-4">
								<p class="noticia-categoria">{item.node.categoria}</p>
								<a href={item.node.ruta} target="_blank"><img src={item.node.img.src} class="img img-fluid" /></a>
								<p class="noticia-titulo">{item.node.title}</p>
								<p class="noticia-leer-mas"><a href={item.node.ruta} target="_blank">Leer más</a></p>
							</div>			
						))}
						</div>
						<div class="row">
							<a href="https://www.boletin.buap.mx/?q=boletines" target="_blank" class="ver-noticias">Ver todas las noticias</a>
						</div>
					</div>
				</div>
			);
		}
	}
}

export default Noticias_Home;
