import React, {Component} from 'react';

/*json.forEach(function(e){
				url_img = e.field_imagen_slider_principal[0].url;	
			});*/
class Eventos_Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			items: []
		};
	}
	
	componentDidMount() {
		fetch(global.config.variables.url_base + "ws_eventos_principal")
		.then(function(response){
			return response.json();
		})
		.then(
			(json) => {
				this.setState({
					isLoaded: true,
					items: json,
				});
			}
		);

	}

	render() {
		const { error, isLoaded, items} = this.state;
		if (error) {
			return <div>Error: {error.message}</div>;
		}
		else if (!isLoaded) {
			return <div>Cargando...</div>;
		}
		else {
			return(
				<div class="row row-eventos">
					<p>Eventos</p>
					<div class="col-lg-12 col-eventos">
						<div class="row">

						{items.map(item=>(
							<div class="col-lg-3">
								<a href="#"><img src={global.config.variables.site_url_base+item.field_imagen_eventos} class="img img-fluid" /></a>
								<div><b><a href="#">{item.title}</a></b></div>
								<div dangerouslySetInnerHTML={{ __html: item.field_comentario_eventos }} /> 
							</div>			
						))}
						</div>
					</div>
				</div>
			);
		}
	}
}

export default Eventos_Home;
