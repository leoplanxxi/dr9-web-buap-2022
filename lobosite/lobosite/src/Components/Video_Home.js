import React, {Component} from 'react';
import {
	Carousel,
	CarouselControl,
	CarouselIndicators,
	CarouselItem
} from 'reactstrap';
/*json.forEach(function(e){
				url_img = e.field_imagen_slider_principal[0].url;	
			});*/
class Video_Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			items: []
		};
	}
	
	componentDidMount() {
		fetch(global.config.variables.url_base + "ws_videoyt_principal")
		.then(function(response){
			return response.json();
		})
		.then(
			(json) => {
				this.setState({
					isLoaded: true,
					items: json,
				});
			}
		);

	}

	render() {
		const { error, isLoaded, items} = this.state;

		if (error) {
			return <div>Error: {error.message}</div>;
		}
		else if (!isLoaded) {
			return <div>Cargando...</div>;
		}
		else {
			return(
				<div class="row row-video-yt">
					<div class="col-lg-12 col-video-yt">
						<Carousel
							activeIndex={0}
							next={function noRefCheck() {}}
							prev={function noRefCheck() {}}
						>
							{items.map(item=>(
								<CarouselItem
									onExited={function noRefCheck() {}}
									onExiting={function noRefCheck() {}}
								>
                                    <iframe src={"https://www.youtube.com/embed/"+item.field_id_video_yt} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</CarouselItem>
							))}
							<CarouselControl
								direction="prev"
								directionText="Anterior"
								onClickHandler={function noRefCheck() {}} />
							<CarouselControl
								direction="next"
								directionText="Siguiente"
								onClickHandler={function noRefCheck() {}} />
						</Carousel>
					</div>
				</div>
			);
		}
	}
}

export default Video_Home;
