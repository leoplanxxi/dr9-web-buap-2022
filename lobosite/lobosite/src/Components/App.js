import '../App.scss';
import '../App.css';
import Header from './Header';
import SliderPrincipal_Home from './SliderPrincipal_Home';
import SliderIntermedio_Home from './SliderIntermedio_Home';
import Buscador_Home from './Buscador_Home';
import Noticias_Home from './Noticias_Home';
import Video_Home from './Video_Home';
import Eventos_Home from './Eventos_Home';

function App() {
  return (
    <div className="App">
    	<Header />	
    	<div class="container container-home">
    		<SliderPrincipal_Home />
    		<Buscador_Home />
    		<SliderIntermedio_Home />
    		<Noticias_Home />
			<Video_Home />
			<Eventos_Home />
    	</div>
    </div>
  );
}

export default App;
