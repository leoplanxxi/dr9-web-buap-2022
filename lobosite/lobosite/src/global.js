module.exports = global.config = {
	variables: {
		url_base: "http://devserver.mhiel.mx/buap-2022/web/",
		site_url_base: "http://devserver.mhiel.mx",
		boletines_url: "https://boletin.buap.mx/?q=ws_boletines_buapmx",
	}
};
